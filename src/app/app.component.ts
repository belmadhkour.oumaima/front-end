import { Component, OnInit  } from '@angular/core';
import { Router } from '@angular/router';
import {NavbarService} from './services/navbar.service';
import {AuthentificationComponent} from './components/authentification/authentification.component';
import {AdminBoardComponent} from './components/espace admin/admin-board/admin-board.component';
import { UserBoardComponent} from './components/espace utilisateur/user-board/user-board.component';
import {TokenStorageService} from './services/token-storage.service';
import{DetailUtilisaturComponent}from './components/espace admin/gestion des utilisateurs/detail-utilisatur/detail-utilisatur.component'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  links: Array<{ text: string, path: string }>;
  isLoggedIn = false;
  constructor(private router: Router, 
              private navbarService: NavbarService,
              private tokenStorageService: TokenStorageService) {
    this.router.config.unshift(
      { path: 'authentification', component:AuthentificationComponent },
      { path: 'admin-board', component: AdminBoardComponent },
      { path: 'utilisateur-board', component: UserBoardComponent }
    );
  }
 
  //Mettre à jour navbar aprés l'authentification
  ngOnInit() {
    this.links = this.navbarService.getLinks();
    this.navbarService.getLoginStatus().subscribe(status => this.isLoggedIn = status);
  }
  
 //Déconnexion méthode

  logout() {
    window.sessionStorage.clear();
    this.navbarService.updateLoginStatus(false);
    this.tokenStorageService.signOut();
    this.router.navigate(['/']);
}
}