//import
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { Routes, RouterModule } from '@angular/router';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import {MatIconModule} from '@angular/material/icon';
import { CommonModule } from '@angular/common';

 

//les components(declarations)
import { AppComponent } from './app.component';
import { AuthentificationComponent } from './components/authentification/authentification.component';
import { AjoutUtilisaturComponent } from './components/espace admin/gestion des utilisateurs/ajout-utilisatur/ajout-utilisatur.component';
import { DetailUtilisaturComponent } from './components/espace admin/gestion des utilisateurs/detail-utilisatur/detail-utilisatur.component';
import { ListeUtilisatursComponent } from './components/espace admin/gestion des utilisateurs/liste-utilisaturs/liste-utilisaturs.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {ListeProdComponent}from './components/espace admin/gestion des produits/liste-prod/liste-prod.component'
import {AjoutProdComponent}from './components/espace admin/gestion des produits/ajout-prod/ajout-prod.component';
import {DetailProdComponent} from './components/espace admin/gestion des produits/detail-prod/detail-prod.component';
import { AdminBoardComponent } from './components/espace admin/admin-board/admin-board.component';
import { UserBoardComponent} from './components/espace utilisateur/user-board/user-board.component';
import { TestTelnetComponent } from './components/espace utilisateur/test-telnet/test-telnet.component';
import { TestDfaComponent } from './components/espace utilisateur/test-dfa/test-dfa.component';
import { TestProduitComponent } from './components/espace utilisateur/test-produit/test-produit.component';
import { Dialogue1Component } from './components/espace utilisateur/dialogue1/dialogue1.component';
import { Dialogue2Component } from './components/espace utilisateur/dialogue2/dialogue2.component';
import { RequestresetComponent } from './components/authentification/requestreset/requestreset.component';
import { ResponseresetComponent } from './components/authentification/responsereset/responsereset.component';
import { AjoutTestComponent } from './components/espace admin/gestion des tests/ajout-test/ajout-test.component';
import { DetailTestComponent } from './components/espace admin/gestion des tests/detail-test/detail-test.component';
import { ListeTestComponent } from './components/espace admin/gestion des tests/liste-test/liste-test.component'

//les services(providers)
import {UtilisateursService} from './services/utilisateurs.service';
import {NavbarService} from './services/navbar.service';
import {TokenStorageService} from './services/token-storage.service';
import {BoardService} from './services/board.service';
import { AuthInterceptor } from './helpers/auth.interceptor';
import {WebsocketService} from './services/websocket.service';


//les paths
const appRoutes: Routes = [
{ path: 'Demande-reinitialisation', component: RequestresetComponent },
{ path: 'Réponse-reinitialisation', component: ResponseresetComponent},

];
@NgModule({

  declarations: [
    AppComponent,
    AuthentificationComponent,
    AjoutUtilisaturComponent,
    DetailUtilisaturComponent,
    ListeUtilisatursComponent,
    AdminBoardComponent,
    UserBoardComponent,
    TestTelnetComponent,
    TestDfaComponent,
    TestProduitComponent,
    Dialogue1Component,
    Dialogue2Component,
    RequestresetComponent,
    ResponseresetComponent,
    AjoutTestComponent,
    DetailTestComponent,
    ListeTestComponent,
    AjoutProdComponent,
    DetailProdComponent,
    ListeProdComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    BrowserModule, 
    FormsModule ,
    HttpClientModule, 
    ReactiveFormsModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    BrowserAnimationsModule,MatIconModule
    ,ToastrModule.forRoot()
  ],

  providers: [
    UtilisateursService,
    NavbarService,
    AuthInterceptor,
    BoardService,
    TokenStorageService,
    WebsocketService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
