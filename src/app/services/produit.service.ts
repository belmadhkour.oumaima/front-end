import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//url globale
const baseUrl = 'http://localhost:8083/api/produits'

//url ajout produit
const baseUrl1 = 'http://localhost:8083/api/produits/ajout'

//liste produit
const baseUrl2 = 'http://localhost:8083/api/produits/listeproduits'

//test
const baseUrl3 = 'http://localhost:8083/api/tests/listetests'

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private http: HttpClient) { }

  //ajout produit
  create(data):Observable<Produits>{
    return this.http.post<Produits>(baseUrl1, data)}

  //Liste des produitss
  getAll():Observable<ProduitsResponse>{
    return this.http.get<ProduitsResponse>(baseUrl2);
  }

  //liste des tests
  getAllTests():Observable<Tests>{
    return this.http.get<Tests>(baseUrl3);
  }
  
  //trouver un produit avec son id
  get(id) {
    return this.http.get(`${baseUrl}/${id}`);
  }

  //Mettre a jour un produit
  update(id, data) :Observable<ProduitsResponse>{
    return this.http.put<ProduitsResponse>(`${baseUrl}/${id}`, data);
  }

  //Supprimer un produit
  delete(id) {
    return this.http.delete(`${baseUrl}/${id}`);
  }
  //Recherche par nomproduit
  findByNomproduit(nomproduit): Observable<ProduitsResponse>{
    return this.http.get<ProduitsResponse>(`${baseUrl}?nomproduit=${nomproduit}`);
  }
}
export class Produits {
  nomproduit: string ;
  tests:[];
};
export class ProduitsResponse {
  message: string;
  ListProduits: Produits[] ;
};
export class Tests {
  nomtest: string;
}

