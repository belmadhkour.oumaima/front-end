import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
//import { retry, catchError } from 'rxjs/operators';

//url globale
const baseUrl = 'http://localhost:8083/api/users'

//url creer un nv utilisateur
const baseUrl1 = 'http://localhost:8083/api/users/signup'

//liste
const baseUrl0 = 'http://localhost:8083/api/users/'

//url authentification
const baseUrl2 = 'http://localhost:8083/api/users/signin'

//reinitialisation
const baseUrl3 = 'http://localhost:8083/api/users/req-reset-password'
const baseUrl4 = 'http://localhost:8083/api/users/new-password'
const baseUrl5 = 'http://localhost:8083/api/users/valid-password-token'

//role
const baseUrl6 = 'http://localhost:8083/api/roles/listeroles'


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class UtilisateursService {

  constructor(private http: HttpClient) { }
  
  //reinitialisation
  requestReset(data){
    return this.http.post(baseUrl3, data);
  }
  
  newPassword(data) {
    return this.http.post(baseUrl4, data);
  }

  ValidPasswordToken(data){
    return this.http.post(baseUrl5, data);
  }

  //authentification
  login(credentials): Observable<any> {
    return this.http.post(baseUrl2, {
      matricule: credentials.matricule,
      password: credentials.password
    }, httpOptions);
  }
  
  //ajout user
  create(data):Observable<Users>{
      return this.http.post<Users>(baseUrl1,data);
  }
  
  //Liste des utilisateurs
  getAll():Observable<UsersResponse>{
    return this.http.get<UsersResponse>(baseUrl0);
  }

  //liste des roles
  getAllRoles(){
    return this.http.get(baseUrl6);
  }
  
  //trouver un user avec son id
  get(id) {
    return this.http.get(`${baseUrl}/${id}`);
  }
  
  //Mettre a jour l'utilisateur
  update(id, data) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  //Supprimer un utilisateur
  delete(id) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  //Recherche par matricule
  findByMatricule(matricule){
      return this.http.get(`${baseUrl}?matricule=${matricule}`);
    }
}
export class Users {
  matricule: string ;
  name: String;
  email: String;
  password: String;
  roles:[];
};
export class UsersResponse {
  message: string;
  ListUtilisateurs: Users[] ;
};