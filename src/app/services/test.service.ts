import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//url globale
const baseUrl = 'http://localhost:8083/api/tests'

//url ajout test
const baseUrl1 = 'http://localhost:8083/api/tests/ajouttest'

//liste test
const baseUrl2 = 'http://localhost:8083/api/tests/listetests'
@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(private http: HttpClient) { }
  
  //ajout test
  create(data){
    return this.http.post(baseUrl1, data)}

  //Liste des tests
  getAll(){
    return this.http.get(baseUrl2);
  }

  
  //trouver un test avec son id
  get(id) {
    return this.http.get(`${baseUrl}/${id}`);
  }
  //Mettre a jour un test
  update(id, data) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  //Supprimer un test
  delete(id) {
    return this.http.delete(`${baseUrl}/${id}`);
  }

  //Recherche par nomtest
  findByNomtest(nomtest){
    return this.http.get(`${baseUrl}?nomtest=${nomtest}`);
  }
}


export class Tests {
  nomtest: string;
}
export class TestsResponse {
  ListTests: Tests[] ;
};