import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import {TokenStorageService} from '../services/token-storage.service';
import { UtilisateursService }from '../services/utilisateurs.service';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  private links = new Array<{ text: string, path: string }>();
  private isLoggedIn = new Subject<boolean>();
  private roles: string[];

  constructor(private tokenStorageService:  TokenStorageService,
             private utilsateurService: UtilisateursService) { 
    this.addItem({ text: 'Authentification', path: 'authentification' });
    this.isLoggedIn.next(false);
  }
  getLinks() {
    return this.links;
  }
 
  getLoginStatus() {
    return this.isLoggedIn;
  }
 
  updateLoginStatus(status: boolean) {
    this.isLoggedIn.next(status);
 
    if (!status) {
      this.clearAllItems();
      this.addItem({ text: 'Authentification', path: 'authentification' });
    }
  }
 
  updateNavAfterAuth(roles: string): void {
    this.removeItem({ text: 'Authentification' });
    const user = this.tokenStorageService.getUser();
    this.roles = this.tokenStorageService.getUser().roles;

    if( this.roles.includes('ROLE_USER')){
      this.addItem({ text: 'Espace Utilisateur', path: 'utilisateur-board' });

    }else if (this.roles.includes('ROLE_ADMIN')){
      this.addItem({ text: 'Espace Admin', path: 'admin-board' });
    }
  }
 
  addItem({ text, path }) {
    this.links.push({ text: text, path: path });
  }
 
  removeItem({ text }) {
    this.links.forEach((link, index) => {
      if (link.text === text) {
        this.links.splice(index, 1);
      }
    });
  }
 
  clearAllItems() {
    this.links.length = 0;
  }
}
