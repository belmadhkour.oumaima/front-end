import { Component, OnInit } from '@angular/core';
import { TestService } from 'src/app/services/test.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ListeTestComponent } from '../liste-test/liste-test.component';

@Component({
  selector: 'app-detail-test',
  templateUrl: './detail-test.component.html',
  styleUrls: ['./detail-test.component.css']
})
export class DetailTestComponent implements OnInit {

  currentTest = null;
  message = '';
  errorMessage = '';
  isFailed = false;

  constructor(private testService: TestService,
    private route : ActivatedRoute,
    private router: Router) { 
    this.router.config.unshift(
          { path: 'listetest', component: ListeTestComponent },
          { path: 'detail_tests/:id', component: DetailTestComponent },
        );}

  ngOnInit(): void {
    this.message = '';
    this.getTest(this.route.snapshot.paramMap.get('id'));
  }
  getTest(id){
    this.testService.get(id)
    .subscribe(
    data => {
      this.currentTest = data;
      console.log(data);
    },
    error => {
      console.log(error);
    });
  }
  updateTest() {
    this.testService.update(this.currentTest.id, this.currentTest)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'Le test a été mis à jour avec succès!';
          this.router.navigate(['/listetests']);
        },
        error => {
          console.log(error);
          this.isFailed = true;
          this.errorMessage = error.error.message;
        });
    } 
    deleteTest() {
    this.testService.delete(this.currentTest.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/listetests']);
        },
        error => {
          console.log(error);
        });
    }
    
    liste(){
      this.router.navigate(['/listetests']);
    }
}
