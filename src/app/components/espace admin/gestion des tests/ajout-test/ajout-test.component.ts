import { Component, OnInit } from '@angular/core';
import { TestService} from 'src/app/services/test.service'
import { Router } from '@angular/router';
@Component({
  selector: 'app-ajout-test',
  templateUrl: './ajout-test.component.html',
  styleUrls: ['./ajout-test.component.css']
})
export class AjoutTestComponent implements OnInit {

  submitted = false;
  form: any = {};
  isSuccessful = false;
  isFailed = false;
  errorMessage = '';
  error: string;
  test = {
   nomtest: '',
  };

  constructor(private testService: TestService,private router:Router) { }

  ngOnInit(): void {
  }
  saveTest() {
    const data = {
      nomtest: this.test.nomtest,
    };

    this.testService.create(data)
      .subscribe(
        response => {
          this.test = data;
          console.log(response);
          this.submitted = true;
        },
        error => {
          this.isFailed = true;
          this.errorMessage = error.error.message;
        
          console.log(error);
        });
  }
  onSubmit() {
    this.router.navigate(['listetests']);
  }

  newTest() {
    this.submitted = false;
    this.test = {
     nomtest: ''
    };
  }


}
