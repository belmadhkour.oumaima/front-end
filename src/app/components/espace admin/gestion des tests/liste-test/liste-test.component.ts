import { Component, OnInit } from '@angular/core';
import { TestService } from 'src/app/services/test.service'
import { Router } from '@angular/router';
import { DetailTestComponent } from '../detail-test/detail-test.component';
import { AjoutTestComponent } from '../ajout-test/ajout-test.component';
@Component({
  selector: 'app-liste-test',
  templateUrl: './liste-test.component.html',
  styleUrls: ['./liste-test.component.css']
})
export class ListeTestComponent implements OnInit {

  tests: any;
  currentTest = null;
  currentIndex = -1;
  nomtest = '';

  constructor(private testservice: TestService,
      private router:Router) { 
        this.router.config.unshift(
          { path: 'listetests', component: ListeTestComponent },
          { path: 'listetests/detail_tests/:id', component: DetailTestComponent},
          { path: 'ajout_test', component: AjoutTestComponent }
        );
      }

  ngOnInit(): void {
    this.retreiveTests();
  }

  onSubmit2() {
    this.router.navigate(['ajout_test']);
  }

  retreiveTests(){
    this.testservice.getAll()
    .subscribe(
      data => {
        this.tests = data;       
        console.log(data);
      },
      error => {
        console.log(error);
      });
  } 
  refreshList() {
    this.retreiveTests();
    this.currentTest = null;
    this.currentIndex = -1;
  }

  setActiveTest(test, index) {
    this.currentTest= test;
    this.currentIndex = index;
  } 

  searchNomtest():void {
    this.testservice.findByNomtest(this.nomtest)
      .subscribe(
        data => {
          this.tests = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
