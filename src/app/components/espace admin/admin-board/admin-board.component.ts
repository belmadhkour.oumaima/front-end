import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {  BoardService} from '../../../services/board.service';

import { ListeUtilisatursComponent  } from '../gestion des utilisateurs/liste-utilisaturs/liste-utilisaturs.component';
import { ListeProdComponent } from '../gestion des produits/liste-prod/liste-prod.component'
import { ListeTestComponent } from '../gestion des tests/liste-test/liste-test.component'
import { DetailUtilisaturComponent} from '../gestion des utilisateurs/detail-utilisatur/detail-utilisatur.component'

@Component({
  selector: 'app-admin-board',
  templateUrl: './admin-board.component.html',
  styleUrls: ['./admin-board.component.css']
})
export class AdminBoardComponent implements OnInit {
  content = '';
  constructor(private board: BoardService,
    private router: Router ) { 
      this.router.config.unshift(
        { path: 'admin-board', component: AdminBoardComponent },
        { path: 'listeutilisateurs',component: ListeUtilisatursComponent },
        { path: 'listeproduits', component: ListeProdComponent },
        { path: 'listetests', component: ListeTestComponent },
        { path: 'detail_utilisateurs/:id', component: DetailUtilisaturComponent },
      );
    }

  ngOnInit() {
    
  }

  reloadPage() {
    window.location.reload();
    
  }
  
  //Interface getion utilisateur: liste + edit + suppression + ajouter
  onSubmit() {
    this.router.navigate(['listeutilisateurs']);
  }
  //Interface gestion produit: liste + edit + suppression + ajout

  onSubmit1() {
  this.router.navigate(['listeproduits']);
  }

  onSubmit2() {
    this.router.navigate(['listetests']);
    }

}
