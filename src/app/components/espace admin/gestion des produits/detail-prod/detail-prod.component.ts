import { Component, OnInit } from '@angular/core';
import { ProduitService } from '../../../../services/produit.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ListeProdComponent  } from '../liste-prod/liste-prod.component';

@Component({
  selector: 'app-detail-prod',
  templateUrl: './detail-prod.component.html',
  styleUrls: ['./detail-prod.component.css']
})
export class DetailProdComponent implements OnInit {

  currentProduit = null;
  message = '';
  errorMessage = '';
  isFailed = false;
  constructor(
    private route : ActivatedRoute,
    private router: Router,
    private produitService: ProduitService) {
      this.router.config.unshift(
        { path: 'listeproduits', component: ListeProdComponent },
        { path: 'detail_produits/:id', component: DetailProdComponent },
      );
     }

  ngOnInit(): void {
    this.message = '';
    this.getProduit(this.route.snapshot.paramMap.get('id'));    
  }

  
  getProduit(id){
    this.produitService.get(id)
    .subscribe(
    data => {
      this.currentProduit = data;
      console.log(data);
    },
    error => {
      console.log(error);
    });
  }

  updateProduit() {
    this.produitService.update(this.currentProduit.id, this.currentProduit)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'Le produit a été mis à jour avec succès!';
          this.router.navigate(['/listeproduits']);
        },
        error => {
          console.log(error);
          this.isFailed = true;
          this.errorMessage = error.error.message;
        });
    }
    
    deleteProduit() {
    this.produitService.delete(this.currentProduit.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/listeproduits']);
        },
        error => {
          console.log(error);
        });
    }
    
    liste(){
      this.router.navigate(['/listeproduits']);
    }
    

}
