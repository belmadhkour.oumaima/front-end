import { Component, OnInit } from '@angular/core';
import { ProduitService} from 'src/app/services/produit.service';
import { Router } from '@angular/router';
import {TokenStorageService} from '../../../../services/token-storage.service';
import { DetailProdComponent } from '../detail-prod/detail-prod.component';
import { AjoutProdComponent } from '../ajout-prod/ajout-prod.component';


@Component({
  selector: 'app-liste-prod',
  templateUrl: './liste-prod.component.html',
  styleUrls: ['./liste-prod.component.css']
})
export class ListeProdComponent implements OnInit {
  products: any;
  currentProduit = null;
  currentIndex = -1;
  nomproduit = '';
  tests: [];

  constructor(
    private prodService: ProduitService,
              private router:Router,
              private tokenStorageService:TokenStorageService
  ) { 
    this.router.config.unshift(
      { path: 'listeproduits', component: ListeProdComponent },
      { path: 'listeproduits/detail_produits/:id', component: DetailProdComponent },
      { path: 'ajout_produit', component: AjoutProdComponent }
    );
  }

  ngOnInit(): void {
    this.retreiveProduits();
  }
  onSubmit2() {
    this.router.navigate(['ajout_produit']);

  }
 
  retreiveProduits(){
    this.prodService.getAll()
    .subscribe(
      data => {
        this.products = data.message;
        this.products = data.ListProduits;
        
        console.log(data.ListProduits);
      },
      error => {
        console.log(error);
      });
  }
  
  refreshList() {
    this.retreiveProduits();
    this.currentProduit = null;
    this.currentIndex = -1;
  }

  setActiveProduit(produit, index) {
    this.currentProduit = produit;
    this.currentIndex = index;
  }

  searchNomproduit() {
    this.prodService.findByNomproduit(this.products.nomproduit)
      .subscribe(
        data => {
          this.products= data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
