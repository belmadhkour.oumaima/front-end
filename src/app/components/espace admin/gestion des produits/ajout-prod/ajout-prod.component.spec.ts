import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutProdComponent } from './ajout-prod.component';

describe('AjoutProdComponent', () => {
  let component: AjoutProdComponent;
  let fixture: ComponentFixture<AjoutProdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutProdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutProdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
