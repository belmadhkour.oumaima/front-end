import { Component, OnInit } from '@angular/core';
import { ProduitService } from 'src/app/services/produit.service'
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-ajout-prod',
  templateUrl: './ajout-prod.component.html',
  styleUrls: ['./ajout-prod.component.css']
})
export class AjoutProdComponent implements OnInit {
  submitted = false;
  form: any = {};
  isSuccessful = false;
  isAjoutFailed = false;
  errorMessage = '';
  error: string;
  produit = {
    nomproduit: '',
    tests: []
  };


  constructor(private produitService: ProduitService,private router:Router) { }

  ngOnInit(): void {
  }

  saveProduit() {
    const data = {
      nomproduit: this.produit.nomproduit,
      tests: [this.produit.tests]
    };
    this.produitService.create(data)
      .subscribe(
        response => {
          this.produit = data;
          console.log(response);
          this.submitted = true;
        },
        error => {
          this.isAjoutFailed = true;
          this.errorMessage = error.error.message;
          console.log(error);
        });
  }
  onSubmit() {
    this.router.navigate(['listeproduits']);
  }

  newProduit() {
    this.submitted = false;
    this.produit = {
      nomproduit: '',
      tests: []
    };
  }

}
