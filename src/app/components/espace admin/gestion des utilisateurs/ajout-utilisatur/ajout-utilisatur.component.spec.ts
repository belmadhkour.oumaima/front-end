import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutUtilisaturComponent } from './ajout-utilisatur.component';

describe('AjoutUtilisaturComponent', () => {
  let component: AjoutUtilisaturComponent;
  let fixture: ComponentFixture<AjoutUtilisaturComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutUtilisaturComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutUtilisaturComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
