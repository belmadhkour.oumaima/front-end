import { Component, OnInit } from '@angular/core';
import { UtilisateursService} from 'src/app/services/utilisateurs.service'
import {FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-ajout-utilisatur',
  templateUrl: './ajout-utilisatur.component.html',
  styleUrls: ['./ajout-utilisatur.component.css']
})
export class AjoutUtilisaturComponent implements OnInit {
  submitted = false;
  form: any = {};
  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';
  error: string;
  user = {
    matricule: '',
    name: '',
    email: '',
    password: '',
    roles:[],
  };
  

  constructor(private utilisateurService: UtilisateursService,private router:Router) { }

  ngOnInit(): void {
  }

  saveUser() {
    const data = {
      matricule: this.user.matricule,
      name: this.user.name,
      email: this.user.email,
      password: this.user.password,
      roles: []
    };

    this.utilisateurService.create(data)
      .subscribe(
        response => {
          this.user = data;
          console.log(response);
          this.submitted = true;
        },
        error => {
          this.isSignUpFailed = true;
          this.errorMessage = error.error.message;
        
          console.log(error);
        });
  }
  onSubmit() {
    this.router.navigate(['listeutilisateurs']);
  }

  newUser() {
    this.submitted = false;
    this.user = {
      matricule: '',
      name: '',
      email: '',
      password: '',
      roles:[],
    };
  }



/* email
email = new FormControl('', [Validators.required, Validators.email]);

  getErrorMessage() {
    if (this.email.hasError('required')) {
      return 'L e-mail est obligatoire';
    }

    return this.email.hasError('email') ? 'Not a valid email' : '';
  }*/
  
 

}
