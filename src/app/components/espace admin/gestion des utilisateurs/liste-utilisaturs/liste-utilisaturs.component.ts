import { Component, OnInit } from '@angular/core';
import { UtilisateursService} from 'src/app/services/utilisateurs.service'
import { Router } from '@angular/router';
import {TokenStorageService} from '../../../../services/token-storage.service';
import { DetailUtilisaturComponent } from '../detail-utilisatur/detail-utilisatur.component';
import { AjoutUtilisaturComponent } from '../ajout-utilisatur/ajout-utilisatur.component';

@Component({
  selector: 'app-liste-utilisaturs',
  templateUrl: './liste-utilisaturs.component.html',
  styleUrls: ['./liste-utilisaturs.component.css']
})
export class ListeUtilisatursComponent implements OnInit {
  
  users: any;
  currentUser = null;
  currentIndex = -1;
  matricule = '';
  roles: [];

  
  constructor(private utilisateurService: UtilisateursService,
              private router:Router,
              private tokenStorageService:TokenStorageService) {
    this.router.config.unshift(
      { path: 'listeutilisateurs', component: ListeUtilisatursComponent },
      { path: 'listeutilisateurs/detail_utilisateurs/:id', component: DetailUtilisaturComponent },
      { path: 'ajout_utilisateur', component: AjoutUtilisaturComponent }
    );
   }

  ngOnInit(): void {
    this.retreiveUsers();
    //this.currentUser = this.tokenStorageService.getUser();
    this.roles = this.tokenStorageService.getUser().roles;
  }

  onSubmit2() {
    this.router.navigate(['ajout_utilisateur']);
  }
 
  retreiveUsers(){
    this.utilisateurService.getAll()
    .subscribe(
      data => {
        this.users = data.message;
        this.users = data.ListUtilisateurs;
        
        console.log(data);
      },
      error => {
        console.log(error);
      });
  }

  refreshList() {
    this.retreiveUsers();
    this.currentUser = null;
    this.currentIndex = -1;
  }

  setActiveUser(user, index) {
    this.currentUser = user;
    this.currentIndex = index;
  }
  
/*
  removeAllUsers() {
    this.utilisateurService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.retreiveUsers();
        },
        error => {
          console.log(error);
        });
  }*/

  searchMatricule():void {
    this.utilisateurService.findByMatricule(this.matricule)
      .subscribe(
        data => {
          this.users = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }



}




