import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeUtilisatursComponent } from './liste-utilisaturs.component';

describe('ListeUtilisatursComponent', () => {
  let component: ListeUtilisatursComponent;
  let fixture: ComponentFixture<ListeUtilisatursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeUtilisatursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeUtilisatursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
