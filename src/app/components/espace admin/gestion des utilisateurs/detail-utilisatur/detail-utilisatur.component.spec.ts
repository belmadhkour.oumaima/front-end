import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailUtilisaturComponent } from './detail-utilisatur.component';

describe('DetailUtilisaturComponent', () => {
  let component: DetailUtilisaturComponent;
  let fixture: ComponentFixture<DetailUtilisaturComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailUtilisaturComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailUtilisaturComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
