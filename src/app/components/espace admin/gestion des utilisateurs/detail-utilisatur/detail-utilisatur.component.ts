import { Component, OnInit } from '@angular/core';
import { UtilisateursService } from 'src/app/services/utilisateurs.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ListeUtilisatursComponent } from '../liste-utilisaturs/liste-utilisaturs.component';

@Component({
  selector: 'app-detail-utilisatur',
  templateUrl: './detail-utilisatur.component.html',
  styleUrls: ['./detail-utilisatur.component.css']
})
export class DetailUtilisaturComponent implements OnInit {

  currentUser = null;
  message = '';
  errorMessage = '';
  isSignUpFailed = false;

  constructor(
    private utilisateurService: UtilisateursService,
    private route : ActivatedRoute,
    private router: Router) { 
    this.router.config.unshift(
          { path: 'listeutilisateurs', component: ListeUtilisatursComponent },
          { path: 'detail_utilisateurs/:id', component: DetailUtilisaturComponent },
        );
    }

  ngOnInit(): void {
    this.message = '';
    this.getUser(this.route.snapshot.paramMap.get('id'));
    
  }

  getUser(id){
    this.utilisateurService.get(id)
    .subscribe(
    data => {
      this.currentUser = data;
      console.log(data);
    },
    error => {
      console.log(error);
    });
}
/*
updatePublished(status) {
const data = {
  matricule: this.currentUser.matricule,
  name: this.currentUser.name,
  email: this.currentUser.email,
  published: status
};

this.utilisateurService.update(this.currentUser.id, data)
  .subscribe(
    response => {
      this.currentUser.published = status;
      console.log(response);
    },
    error => {
      console.log(error);
    });
}
*/
updateUser() {
this.utilisateurService.update(this.currentUser.id, this.currentUser)
  .subscribe(
    response => {
      console.log(response);
      this.message = 'L utilisateur a été mis à jour avec succès!';
      this.router.navigate(['/listeutilisateurs']);
    },
    error => {
      console.log(error);
      this.isSignUpFailed = true;
      this.errorMessage = error.error.message;
    });
}

deleteUser() {
this.utilisateurService.delete(this.currentUser.id)
  .subscribe(
    response => {
      console.log(response);
      this.router.navigate(['/listeutilisateurs']);
    },
    error => {
      console.log(error);
    });
}

liste(){
  this.router.navigate(['/listeutilisateurs']);
}

}
