import { Component, OnInit } from '@angular/core';
import {  BoardService} from '../../../services/board.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-board',
  templateUrl: './user-board.component.html',
  styleUrls: ['./user-board.component.css']
})
export class UserBoardComponent implements OnInit {

  content = '';
  constructor(private board: BoardService  ) { }


  ngOnInit(): void {
    this.board.getUserBoard().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      }
    );
  }
  
}
