import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestDfaComponent } from './test-dfa.component';

describe('TestDfaComponent', () => {
  let component: TestDfaComponent;
  let fixture: ComponentFixture<TestDfaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestDfaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestDfaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
