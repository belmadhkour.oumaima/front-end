import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, _closeDialogVia } from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';



@Component({
  selector: 'app-dialogue2',
  templateUrl: './dialogue2.component.html',
  styleUrls: ['./dialogue2.component.css']
})
export class Dialogue2Component implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<Dialogue2Component>,
    @Inject(MAT_DIALOG_DATA) public data: Dialogue2Component,
    private router: Router, private formBuilder: FormBuilder ) {
  }
  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  dfa: string = "";


  /*conect(a) {

    const Dfa = 'dfa:::dfa';

    if (this.dfa.trim() == Dfa) {
      this.onNoClick();
       this.router.navigate(['/test_produit']);

    }
    else {


    }

  }*/
}
