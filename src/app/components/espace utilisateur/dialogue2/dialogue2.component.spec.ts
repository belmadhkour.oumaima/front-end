import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Dialogue2Component } from './dialogue2.component';

describe('Dialogue2Component', () => {
  let component: Dialogue2Component;
  let fixture: ComponentFixture<Dialogue2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Dialogue2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Dialogue2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
