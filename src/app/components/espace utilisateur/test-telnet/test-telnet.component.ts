import { Component, OnInit ,Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Dialogue1Component } from '../dialogue1/dialogue1.component';
@Component({
  selector: 'app-test-telnet',
  templateUrl: './test-telnet.component.html',
  styleUrls: ['./test-telnet.component.css']
})
export class TestTelnetComponent implements OnInit {

  constructor( public dialog: MatDialog) { }

  ngOnInit(): void {
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(Dialogue1Component, {
     
    });

    dialogRef.afterClosed().subscribe(result => {
    
    });
  }
}
