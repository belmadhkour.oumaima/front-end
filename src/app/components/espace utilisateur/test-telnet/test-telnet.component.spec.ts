import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestTelnetComponent } from './test-telnet.component';

describe('TestTelnetComponent', () => {
  let component: TestTelnetComponent;
  let fixture: ComponentFixture<TestTelnetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestTelnetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTelnetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
