import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestProduitComponent } from './test-produit.component';

describe('TestProduitComponent', () => {
  let component: TestProduitComponent;
  let fixture: ComponentFixture<TestProduitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestProduitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestProduitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
