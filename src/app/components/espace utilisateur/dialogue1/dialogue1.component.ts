import { Component, OnInit , Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Router} from '@angular/router';
import {Dialogue2Component} from '../dialogue2/dialogue2.component';
import { WebsocketService } from '../../../services/websocket.service';

@Component({
  selector: 'app-dialogue1',
  templateUrl: './dialogue1.component.html',
  styleUrls: ['./dialogue1.component.css']
})
export class Dialogue1Component implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<Dialogue1Component>,public dialog2: MatDialog , 
    private router : Router , private webSocketService : WebsocketService
  
    ) {}

    ngOnInit(){
      this.webSocketService.listen('chat').subscribe((data) => this.updateMessage(data));
    }
    
    //
    updateMessage(data:any) {
      if(!!!data) return;
      console.log(`${data.handle} : ${data.message}`);
      this.output.push(data);
    }

    //
  onNoClick(): void {
    this.dialogRef.close();
  }
  host: string = "";
  port: string = "";
  output: any[] = [];

  messageTyping(): void {
    console.log(`${this.host} is typing`) 
  }


  conect(f) {

     const port = '23' ;
     const host= '192.168.5.1';
     

   if (this.host.trim() == host && this.port.trim() == port) {
    console.log({
      message: this.port,
      handle: this.host,
    });
   
    this.onNoClick();
    this.router.navigate(['/test_dfa']);

    //dd2
    const dialogRef = this.dialog2.open(Dialogue2Component, {
    });
    dialogRef.afterClosed().subscribe(result => {
  });

   }
   //port incorrect
   else 
   if(this.host.trim()== host && this.port.trim() !== port)
    {  
    console.log({
      message: this.port,
    });

      //this.toastr.error('port :/ ');
    alert("Port n'est pas correct");
     this.onNoClick()
   }
   //host incorrect 
   else
   if(this.host.trim() !== host && this.port.trim() == port)
    {  
    console.log({
      handle: this.host,
    });
  
    alert("Host n'est pas correct");
     this.onNoClick()
   }
   //port && host incorrect 
   else
   if(this.host.trim() !== host && this.port.trim() !== port)
   {  
   console.log({
     message: this.port,
     handle: this.host,
   });
 
   alert("Host et Port ne sont pas correct ");
    this.onNoClick()
  }
   }
   
}
