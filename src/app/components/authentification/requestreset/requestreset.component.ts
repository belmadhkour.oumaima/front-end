import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl, ReactiveFormsModule } from '@angular/forms';
import {UtilisateursService}from '../../../services/utilisateurs.service';
import {Router } from '@angular/router';
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-requestreset',
  templateUrl: './requestreset.component.html',
  styleUrls: ['./requestreset.component.css']
})

export class RequestresetComponent implements OnInit {
  RequestResetForm: FormGroup;
  forbiddenEmails: any;
  errorMessage: string;
  successMessage: string;
  IsvalidForm = true;

  constructor(
    private authService: UtilisateursService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.RequestResetForm = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email], this.forbiddenEmails),
  });
  }

  RequestResetUser(form){
    console.log(form)
    if (form.valid){
      this.IsvalidForm = true;
      this.authService.requestReset(this.RequestResetForm.value).subscribe(
        data => {
          this.RequestResetForm.reset();
          this.successMessage = "Lien de réinitialisation de mot de passe envoyé à l'email avec succès.";
          setTimeout( () => {
            this.successMessage = null;
            this.router.navigate(['authentification']);
          }, 3000)
        },
        err => {
          if (err.error.message) {
            this.errorMessage = err.error.message;
          }
        }
          );
    }else{
        this.IsvalidForm = false;
      }
  }
}