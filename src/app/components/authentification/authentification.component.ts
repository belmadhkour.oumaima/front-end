import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { UtilisateursService } from '../../services/utilisateurs.service';
import { TokenStorageService } from '../../services/token-storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';

//reinitialision mot de passse
import { RequestresetComponent } from './requestreset/requestreset.component';
import { ResponseresetComponent } from './responsereset/responsereset.component'




@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.css']
})
export class AuthentificationComponent implements OnInit {

  form: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  submitted = false;
  roles: string[] = [];


  constructor(private navbarService: NavbarService,
    private utilisateurService: UtilisateursService,
    private tokenStorageService: TokenStorageService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.router.config.unshift(
      { path: 'Demande-reinitialisation', component: RequestresetComponent },
      { path: 'Réponse-reinitialisation', component: ResponseresetComponent},

    );

    this.navbarService.getLoginStatus().
      subscribe(status => this.isLoggedIn = status);
  }

  ngOnInit(): void {
    if (this.tokenStorageService.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorageService.getUser().roles;
    }

  }

  mdp(){
    this.router.navigate(['Demande-reinitialisation']);
  }

  /*onLogin(matricule, password) {
    if ( matricule === 'admin' && password === 'admin' ){
      this.navbarService.updateNavAfterAuth('user');
      this.navbarService.updateLoginStatus(true);
      //this.role = 'user';
    }}*/

  loginUser() {

    this.utilisateurService.login(this.form).subscribe(
      data => {
        this.tokenStorageService.saveToken(data.accessToken);
        this.tokenStorageService.saveUser(data);

        //this.navbarService.updateNavAfterAuth('user');
        //this.navbarService.updateLoginStatus(true);
        // this.roles = 'user';

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorageService.getUser().roles;


        if (this.roles.includes('ROLE_ADMIN')) {
          this.navbarService.updateNavAfterAuth('admin');
          this.navbarService.updateLoginStatus(true);
          this.router.navigate(['admin-board']);
        }
        else if (this.roles.includes('ROLE_USER')) {
          this.navbarService.updateLoginStatus(true);
          this.navbarService.updateNavAfterAuth('user');
          this.router.navigate(['/test_telnet']);
        }

        else {
          console.log('error');

        }

      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
        console.log(err);
      }
    );
    this.navbarService.getLoginStatus().
      subscribe(status => this.isLoggedIn = status);
  }



  /*
   loginAdmin() {
     this.navbarService.updateNavAfterAuth('admin');
     this.navbarService.updateLoginStatus(true);
     this.role = 'admin';
   }
   
   onSubmit() {//loginUser
     this.utilisateurService.login(this.form).subscribe(
       data => {
         this.navbarService.updateNavAfterAuth('user');
         this.navbarService.updateLoginStatus(true);
         this.role = 'user';
         this.isLoginFailed = false;
         this.isLoggedIn = true;
       },
       err => {
         this.errorMessage = err.error.message;
         this.isLoginFailed = true;
       }
     );
   }
 */


}
